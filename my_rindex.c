/*
 * return pointer to last occurrence of char c in string s
 */

#include <stdio.h>

char * my_rindex(char *, char);
int my_strlen(char *);

int main(int argc, char **argv) {

  if (argc < 3) {
    printf("Usage: %s <string> <letter>\n", argv[0]);
    return 0;
  }

  char *c = NULL;

  c = my_rindex(argv[1], argv[2][0]);
  printf("Return pointer to first occurrence of char\n");
  printf("Seeking first occurrence of char: '%c'\n", argv[2][0]);
  printf("In string: '%s'\n", argv[1]);
  printf("Address of string: %p\n", argv[1]);
  printf("Pointer to first occurence: %p\n", c);

  return 0;
}

int my_strlen(char *s) {
  char *tmp = s;
  if (s == NULL) {
    return -1;
  }

  while (*s != '\0') {
    s++;
  }

  return s - tmp;
}

char * my_rindex(char *s, char c) {
  if (s == NULL) {
    return NULL;
  }
  int len = my_strlen(s);
  char *tmp = s;

  while (*s != '\0') {
    s++;
  }
  while (*s != c) {
    s--;
  }

  return (s - tmp) > len ? 0 : s;
}
